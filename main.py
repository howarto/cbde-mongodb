from pymongo import MongoClient

print('Running...')

DB_CLIENT = MongoClient('localhost', 27017)

def setUpDB(client) :
    client.drop_database('database')

def tearDownDB(client) :
    client.drop_database('database')

def createInsertQ2(values) :
    # print(values)
    query = {
        "ps_key": values['ps_key_value'],
        "ps_supplycost": values['ps_supplycost_value'],
        "ps_suppkey": {
            "s_suppkey": values['s_suppkey_value'],
            "s_acctbal": values['s_acctbal_value'],
            "s_name": 'name',
            "s_address": 'address',
            "s_phone": 'phone',
            "s_comment": 'comment',
            "s_nationkey": {
                "n_nationkey": 'nationkey',
                "n_name": 'name',
                "n_regionkey": {
                    "r_regionkey": 'regionkey',
                    "r_name": values['r_name_value']
                }
            }
        },
        "ps_partkey": {
            "p_partkey": 'partkey',
            "p_mfgr": 'mfgr',
            "p_type": values['p_type_value'],
            "p_size": values['p_size_value']
        }
    }
    return query

def insertQ2(collection) :
    ps_key_values = ['partkey1;suppkey1', 'partkey2;suppkey2', 'partkey1;suppkey2', 'partkey2;suppkey1']
    ps_supplycost_values = [22, 40.0, 30, 50.2]
    s_suppkey_values = ['suppkey1', 'suppkey2', 'suppkey2', 'suppkey1']
    s_acctbal_values = [155.5, 288.8, 399.9, 400.3]
    r_name_values = ['region1', 'region2', 'region1', 'region2']
    p_type_values = ['Atype', 'Btype', 'Ctype', 'Dtype']
    p_size_values = [1, 2, 3, 4]

    insertsArray = []
    for index in range(0, len(ps_key_values)):
        insertion = createInsertQ2({
            "ps_key_value": ps_key_values[index],
            "ps_supplycost_value": ps_supplycost_values[index],
            "s_suppkey_value": s_suppkey_values[index],
            "s_acctbal_value": s_acctbal_values[index],
            "r_name_value": r_name_values[index],
            "p_type_value": p_type_values[index],
            "p_size_value": p_size_values[index],
        })
        # print(insertion)
        insertsArray.append(insertion)

    # print(insertsArray)
    collection.insert_many(insertsArray)

def createInsertQ134(values) :
    # print(values)
    query = {
        "l_linenumber": values['l_linenumber_value'],
        "l_returnflag": values['l_returnflag_value'],
        "l_linestatus": 'linestatus',
        "l_quantity": values['l_quantity_value'],
        "l_extendedprice": values['l_extendedprice_value'],
        "l_discount": values['l_discount_value'],
        "l_tax": values['l_tax_value'],
        "l_shipdate": values['l_shipdate_value'],
        "l_suppkey": {
            "s_suppkey": 'suppkey',
            "s_nationkey": {
                "n_nationkey": 'nationkey',
                "n_name": 'name',
                "n_regionkey": {
                    "r_regionkey": 'region',
                    "r_name": values['r_name_value']
                }
            }
        },
        "l_orderkey": {
            "o_orderkey": 'orderkey',
            "o_orderdate": values['o_orderdate_value'],
            "o_shippriority": values['o_shippriority_value'],
            "o_custkey": {
                "c_custkey": 'custkey',
                "c_mktsegment": 'mktsegment',
                "c_nationkey": {
                    "n_nationkey": 'nationkey',
                    "n_name": 'name',
                    "n_regionkey": {
                        "r_regionkey": 'region',
                        "r_name": values['r_name_value']
                    }
                }
            }
        }
    }
    return query

def insertQ134(collection) :
    l_linenumber_values = [1, 2, 3, 4]
    l_quantity_values = [10, 11, 12, 13]
    l_extendedprice_values = [5, 10, 15, 20]
    l_discount_values = [0.1, 0.2, 0.5, 0.8]
    l_tax_values = [1, 2, 3, 4,]
    l_shipdate_values = [20, 21, 21, 25]
    r_name_values = ['region1', 'region2', 'region1', 'region2']
    o_orderdate_values = [14, 15, 15, 17]
    o_shippriority_values = [0, 1, 0, 1]
    l_returnflag_values = ['returnflag1', 'returnflag2', 'returnflag1', 'returnflag1']

    insertsArray = []
    for index in range(0, len(l_linenumber_values)):
        insertion = createInsertQ134({
            "l_linenumber_value": l_linenumber_values[index],
            "l_quantity_value": l_quantity_values[index],
            "l_extendedprice_value": l_extendedprice_values[index],
            "l_discount_value": l_discount_values[index],
            "l_tax_value": l_tax_values[index],
            "l_shipdate_value": l_shipdate_values[index],
            "r_name_value": r_name_values[index],
            "o_orderdate_value": o_orderdate_values[index],
            "o_shippriority_value": o_shippriority_values[index],
            "l_returnflag_value": l_returnflag_values[index]
        })
        # print(insertion)
        insertsArray.append(insertion)

    # print(insertsArray)
    collection.insert_many(insertsArray)

def query1(collection, date) :
    match = {
        "$match": {
            "l_shipdate": {
                "$lte": date
            }
        }
    }

    group = {
        "$group": {
            "_id": {
                "l_returnflag": "$l_returnflag",
                "l_linestatus": "$l_linestatus"
            },
            "sum_qty": {
                "$sum": "$l_quantity"
            },
            "sum_base_price": {
                "$sum": "$l_extendedprice"
            }
        }
    }

    sort = {
        "$sort": {
            "l_returnflag": 1,
            "l_linestatus": 1
        }
    }

    aggregation = aggregate(collection, [match, group, sort])
    print(aggregation)

def query2(collection, size, p_type, region) :
    # Subquery
    match = {
        "$match": {
            "ps_suppkey.s_nationkey.n_regionkey.r_name": region
        }
    }

    group = {
        "$group": {
            "_id": 0,
            "minpscost": {
                "$min": "$ps_supplycost"
            } 
        }
    }

    subqueryAggregation = aggregate(collection, [match, group])
    minpscost = subqueryAggregation[0]['minpscost']
    print('Subquery Query2 result: ' + str(minpscost))
    # Main query
    match = {
        "$match" : {
            "ps_partkey.p_size" : size,
            "ps_partkey.p_type": {
                "$regex": '.*' + p_type,
                "$options": 'g'
            },
            "ps_suppkey.s_nationkey.n_regionkey.r_name": region,
            "ps_supplycost": minpscost
        }
    }

    project = {
        "$project": {
            "_id": 0,
            "ps_suppkey.s_acctbal": 1,
            "ps_suppkey.s_name": 1,
            "ps_suppkey.s_nationkey.n_name": 1,
            "ps_suppkey.p_partkey": 1,
            "ps_partkey.p_mfgr": 1,
            "ps_suppkey.s_address": 1,
            "ps_suppkey.s_phone": 1,
            "ps_suppkey.s_comment": 1
        }
    }

    sort = {
        "$sort": {
            "ps_suppkey.s_acctbal": -1,
            "ps_suppkey.s_nationkey.n_name": 1,
            "ps_suppkey.s_name": 1,
            "ps_suppkey.p_partkey": 1
        }
    }
    aggregation = aggregate(collection, [match, project, sort])
    print(list(aggregation))

def query3(collection, segment, date1, date2) :
    match = {
        "$match": {
            "l_orderkey.o_custkey.c_mktsegment": segment,
            "l_orderkey.o_orderdate": {
                "$lt": date1
            },
            "l_shipdate": {
                "$gt": date2
            }
        }
    }

    group = {
        "$group": {
            "_id": {
                "l_orderkey": "$l_orderkey",
                "o_orderdate": "$l_orderkey.o_orderdate",
                "o_shippriority": "$l_orderkey.o_shippriority"
            },
            "revenue": {
                "$sum": {
                    "$multiply": [
                        {
                            "$subtract": [1, "$l_discount"]
                        },
                        "$l_extendedprice"
                    ]
                }
            }
        }
    }

    sort = {
        "$sort": {
            "revenue": -1,
            "o_orderdate": 1
        }
    }

    aggregation = aggregate(collection, [match, group, sort])
    print(aggregation)

def query4(collection, date, region) :
    match = {
        "$match": {
            "l_suppkey.s_nationkey.n_regionkey.r_name": region,
            "$expr": {
                "$eq": ["$l_orderkey.o_custkey.c_nationkey.n_nationkey", "$l_suppkey.s_nationkey.n_nationkey"]
            },
            "l_orderkey.o_orderdate": {
                "$gte": date
            },
            "l_orderkey.o_orderdate": {
                # We assume that the dates are numbers
                "$lt": date + 1
            }
        }
    }

    group = {
        "$group": {
            "_id": {
                "n_name": "$l_suppkey.s_nationkey.n_name"
            },
            "revenue": {
                "$sum": {
                    "$multiply": [
                        {"$subtract": [1, "$l_discount"]},
                        "$l_extendedprice"
                    ]
                }
            }
        }
    }

    sort = {
        "$sort": {
            "revenue": -1
        }
    }

    aggregation = aggregate(collection, [match, group, sort])
    print(aggregation)

def aggregate(collection, toAggregation) :
    result = collection.aggregate(toAggregation)
    return list(result)

def main() :
    print('Creating database and making insertions...')
    setUpDB(DB_CLIENT)
    print('DONE')
    
    collectionOne = DB_CLIENT.database.collectionOne
    insertQ134(collectionOne)
    print('')
    print('Query1 execution...')
    query1(collectionOne, 22)
    print('DONE')
    print('')
    print('Query3 execution...')
    query3(collectionOne, 'mktsegment', 16, 20)
    print('DONE')
    print('')
    print('Query4 execution...')
    query4(collectionOne, 15, 'region1')
    print('DONE')
    print('')

    collectionTwo = DB_CLIENT.database.collectionTwo
    insertQ2(DB_CLIENT.database.collectionTwo)
    print('Query2 execution...')
    query2(collectionTwo, 2, 'type', 'region2')
    print('')

    print('Deleting database...')
    tearDownDB(DB_CLIENT)
    print('DONE')
    print('')

# Run the main function
main()