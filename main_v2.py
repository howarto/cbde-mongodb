from pymongo import MongoClient

print('Running...')

DB_CLIENT = MongoClient('localhost', 27017)

def setUpDB(client) :
    client.drop_database('database')

def tearDownDB(client) :
    client.drop_database('database')

def createInsertQ2(values) :
    # print(values)
    query = {
        "_id": values['id_value'],
        "Partsup": {
        	"ps_supplycost": values['ps_supplycost_value']
        },
        "Supplier": {
	        "s_acctbal": values['s_acctbal_value'],
	        "s_name": 'name',
	        "s_address": 'address',
	        "s_phone": 'phone',
	        "s_comment": 'comment'
	    },
	    "Part": {
	        "p_partkey": 'partkey',
	        "p_mfgr": 'mfgr',
	        "p_type": values['p_type_value'],
	        "p_size": values['p_size_value'],
	    },
	    "Nation": {
        	"n_name": 'name'
       	},
       	"Region": {
        	"r_name": values['r_name_value']
        }
    }

    return query

def insertQ2(collection) :
    id_values = ['partkey1;suppkey1', 'partkey1;suppkey2', 'partkey2;suppkey1', 'partkey2;suppkey2', 'partkey3;suppkey1', 'partkey3;suppkey2', 'partkey4;suppkey1', 'partkey4;suppkey2', 'partkey5;suppkey1', 'partkey5;suppkey2']
    ps_supplycost_values = [22, 22, 22, 22, 22, 22, 22, 40.0, 22, 50.2]
    s_acctbal_values = [155.5, 288.8, 399.9, 400.3, 100.0, 200.0, 300.0, 100.0, 155.5, 288.8]
    r_name_values = ['region1', 'region1', 'region1', 'region1', 'region1', 'region2', 'region2', 'region2', 'region2', 'region3']
    p_type_values = ['Atype', 'Atype', 'Atype', 'Dtype', 'Btype', 'Atype', 'Ctype', 'Atype', 'Atype', 'Atype']
    p_size_values = [1, 1, 1, 1, 2, 2, 1, 1, 2, 1]

    insertsArray = []
    for index in range(0, len(id_values)):
        insertion = createInsertQ2({
            "id_value": id_values[index],
            "ps_supplycost_value": ps_supplycost_values[index],
            "s_acctbal_value": s_acctbal_values[index],
            "r_name_value": r_name_values[index],
            "p_type_value": p_type_values[index],
            "p_size_value": p_size_values[index]
        })
        # print(insertion)
        insertsArray.append(insertion)

    # print(insertsArray)
    collection.insert_many(insertsArray)

def createInsertQ134(values) :
    # print(values)
    query = {
    	"_id": values['id_value'],
    	"Lineitem": {
    		"l_orderkey": values['l_orderkey_value'],
		    "l_returnflag": values['l_returnflag_value'],
		    "l_linestatus": values['l_linestatus_value'],
		    "l_quantity": values['l_quantity_value'],
		    "l_extendedprice": values['l_extendedprice_value'],
		    "l_discount": values['l_discount_value'],
		    "l_tax": values['l_tax_value'],
		    "l_shipdate": values['l_shipdate_value'],
		},
	    "Order": {
	        "o_orderdate": values['o_orderdate_value'],
	        "o_shippriority": values['o_shippriority_value']
	    },
	    "Customer": {
	        "c_mktsegment": values['c_mktsegment_value'],
	        "c_nationkey": values['c_nationkey_value']
	    },
	    "Supplier": {
        	"s_nationkey": values['s_nationkey_value']
        },
        "Nation": {
        	"n_name": values['n_name_value']
        },
        "Region": {
        	"r_name": values['r_name_value']
        }
    }
        
    return query

def insertQ134(collection) :
	id_values = ['l_orderkey1;l_linenumber1', 'l_orderkey1;l_linenumber2', 'l_orderkey1;l_linenumber3', 'l_orderkey1;l_linenumber4', 'l_orderkey1;l_linenumber5', 'l_orderkey2;l_linenumber1', 'l_orderkey2;l_linenumber2', 'l_orderkey2;l_linenumber3', 'l_orderkey2;l_linenumber4', 'l_orderkey2;l_linenumber5']
	l_orderkey_values = ['ok1', 'ok1', 'ok1', 'ok1', 'ok1', 'ok2', 'ok2', 'ok2', 'ok2', 'ok2']
	l_quantity_values = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
	l_extendedprice_values = [5, 10, 15, 20, 25, 30, 35, 40, 45, 50]
	l_discount_values = [0.1, 0.2, 0.5, 0.8, 1.2, 1.3, 1.5, 1.6, 1.4, 1.7]
	l_tax_values = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
	l_shipdate_values = [20, 21, 20, 25, 30, 20, 23, 10, 3, 50]
	r_name_values = ['region1', 'region2', 'region1', 'region2', 'region2', 'region2', 'region2', 'region2', 'region3', 'region3']
	o_orderdate_values = [14, 15, 15, 17, 20, 10, 12, 20, 14, 17]
	o_shippriority_values = [0, 1, 0, 1, 1, 1, 2, 3, 4, 5]
	l_returnflag_values = ['rf1', 'rf1', 'rf1', 'rf2', 'rf3', 'rf4', 'rf5', 'rf6', 'rf8', 'rf8']
	l_linestatus_values = ['ls1', 'ls1', 'ls1', 'ls2', 'ls3', 'ls4', 'ls5', 'ls6', 'ls8', 'ls8']
	c_mktsegment_values = ['mktseg1', 'mktseg2', 'mktseg3', 'mktseg2', 'mktseg1', 'mktseg3', 'mktseg1', 'mktseg1', 'mktseg2', 'mktseg4']
	c_nationkey_values = ['Spain', 'Spain', 'Spain', 'Italy', 'Spain', 'Italy', 'Spain', 'Lala', 'Lala', 'Lala']
	s_nationkey_values = ['Spain', 'Spain', 'Spain', 'Italy', 'Italy', 'Italy', 'Italy', 'Lala', 'Lala', 'Lala']
	n_name_values = ['Spain', 'Spain', 'Spain', 'Italy', 'Spain', 'Italy', 'Spain', 'Lala', 'Lala', 'Lala']

	insertsArray = []
	for index in range(0, len(id_values)):
		insertion = createInsertQ134({
			"id_value": id_values[index],
			"l_orderkey_value": l_orderkey_values[index],
			"l_quantity_value": l_quantity_values[index],
			"l_extendedprice_value": l_extendedprice_values[index],
			"l_discount_value": l_discount_values[index],
			"l_tax_value": l_tax_values[index],
			"l_shipdate_value": l_shipdate_values[index],
			"r_name_value": r_name_values[index],
			"o_orderdate_value": o_orderdate_values[index],
			"o_shippriority_value": o_shippriority_values[index],
			"l_returnflag_value": l_returnflag_values[index],
			"l_linestatus_value": l_linestatus_values[index],
			"c_mktsegment_value": c_mktsegment_values[index],
			"c_nationkey_value": c_nationkey_values[index],
			"s_nationkey_value": s_nationkey_values[index],
			"n_name_value": n_name_values[index],
			"r_name_value": r_name_values[index]
		})

		# print(insertion)
		insertsArray.append(insertion)

	# print(insertsArray)
	collection.insert_many(insertsArray)

def query1(collection, date) :
	match = {
		"$match": {
			"Lineitem.l_shipdate": {
				"$lte": date
			}
		}
	}

	group = {
		"$group": {
			"_id": {
				"l_returnflag": "$Lineitem.l_returnflag",
				"l_linestatus": "$Lineitem.l_linestatus"
			},
			"sum_qty": {
				"$sum": "$Lineitem.l_quantity"
			},
			"sum_base_price": {
				"$sum": "$Lineitem.l_extendedprice"
			},
			"sum_disc_price": {
				"$sum": {
					"$multiply": [
						{
							"$subtract": [1, "$Lineitem.l_discount"]
						},
						"$Lineitem.l_extendedprice"
					]
				}
			},
			"sum_charge": {
				"$sum": {
					"$multiply": [
						{
							"$add": [1, "$Lineitem.l_tax"]
						},
						{
							"$multiply": [
								{
									"$subtract": [1, "$Lineitem.l_discount"]
								},
								"$Lineitem.l_extendedprice"
							]
						}
					]
				}
			},
			"avg_qty": {
				"$avg": "$Lineitem.l_quantity"
			},
			"avg_price": {
				"$avg": "$Lineitem.l_extendedprice"
			},
			"avg_disc": {
				"$avg": "$Lineitem.l_discount"
			}
		}
	}

	count = {
		"$count": {
			"l_returnflag",
			"l_linestatus"
		}
	}

	sort = {
		"$sort": {
			"l_returnflag": 1,
			"l_linestatus": 1
		}
	}

	aggregation = aggregate(collection, [match, group, sort])
	print(aggregation)

def query2(collection, size, p_type, region) :
	# Subquery
	match = {
		"$match": {
			"Region.r_name": region
		}
	}

	group = {
		"$group": {
			"_id": 0,
			"minpscost": {
				"$min": "$Partsup.ps_supplycost"
			} 
		}
	}

	subqueryAggregation = aggregate(collection, [match, group])
	print('subqueryAggregation: ', subqueryAggregation)
	minpscost = subqueryAggregation[0]["minpscost"]
	print('Subquery Query2 result: ' + str(minpscost))
	# Main query
	match = {
		"$match" : {
			"Part.p_size" : size,
			"Part.p_type": {
				"$regex": '.*' + p_type,
				"$options": 'g'
			},
			"Region.r_name": region,
			"Partsup.ps_supplycost": minpscost
		}
	}
	

	project = {
		"$project": {
			"_id": 0,
			"Supplier.s_acctbal": 1,
			"Supplier.s_name": 1,
			"Nation.n_name": 1,
			"Part.p_partkey": 1,
			"Part.p_mfgr": 1,
			"Supplier.s_address": 1,
			"Supplier.s_phone": 1,
			"Supplier.s_comment": 1
		}
	}

	sort = {
		"$sort": {
			"Supplier.s_acctbal": -1,
			"Nation.n_name": 1,
			"Supplier.s_name": 1,
			"Part.p_partkey": 1
		}
	}
	aggregation = aggregate(collection, [match, project, sort])
	print(list(aggregation))

def query3(collection, segment, date1, date2) :
	match = {
		"$match": {
			"Customer.c_mktsegment": segment,
			"Order.o_orderdate": {
				"$lt": date1
			},
			"Lineitem.l_shipdate": {
				"$gt": date2
			}
		}
	}

	group = {
		"$group": {
		"_id": {
			"l_orderkey": "$Lineitem.l_orderkey",
			"o_orderdate": "$Order.o_orderdate",
			"o_shippriority": "$Order.o_shippriority"
		},
		"revenue": {
			"$sum": {
				"$multiply": [
				{
					"$subtract": [1, "$Lineitem.l_discount"]
				},
				"$Lineitem.l_extendedprice"
				]
			}
		}
		}
	}

	sort = {
		"$sort": {
			"revenue": -1,
			"o_orderdate": 1
		}
	}

	aggregation = aggregate(collection, [match, group, sort])
	print(aggregation)

def query4(collection, date, region) :
	match = {
		"$match": {
			"Region.r_name": region,
			"$expr": {
				"$eq": ["$Supplier.s_nationkey", "$Customer.c_nationkey"]
			},
			"Order.o_orderdate": {
				"$gte": date
			},
			"Order.o_orderdate": {
			# We assume that the dates are numbers
			"$lt": date + 1
			}
		}
	}

	group = {
		"$group": {
			"_id": {
				"n_name": "$Nation.n_name"
			},
			"revenue": {
				"$sum": {
					"$multiply": [
						{"$subtract": [1, "$Lineitem.l_discount"]},
						"$Lineitem.l_extendedprice"
					]
				}
			}
		}
	}

	sort = {
		"$sort": {
			"revenue": -1
		}
	}

	aggregation = aggregate(collection, [match, group, sort])
	print(aggregation)

def aggregate(collection, toAggregation) :
	result = collection.aggregate(toAggregation)
	return list(result)

def main() :
	print('Creating database and making insertions...')
	setUpDB(DB_CLIENT)
	print('DONE')

	collectionOne = DB_CLIENT.database.collectionOne
	insertQ134(collectionOne)
	print('')
	print('Query1 execution...')
	query1(collectionOne, 22)
	print('DONE')
	print('')
	print('Query3 execution...')
	query3(collectionOne, 'mktseg1', 16, 20)
	print('DONE')
	print('')
	print('Query4 execution...')
	query4(collectionOne, 15, 'region1')
	print('DONE')
	print('')

	collectionTwo = DB_CLIENT.database.collectionTwo
	insertQ2(DB_CLIENT.database.collectionTwo)
	print('Query2 execution...')
	query2(collectionTwo, 2, 'Atype', 'region2')
	print('')

	print('Deleting database...')
	tearDownDB(DB_CLIENT)
	print('DONE')
	print('')

# Run the main function
main()
